vcl 4.0;

backend default {
    .host = "app";
    .port = "3000";
}


acl purge {
        "localhost";
        "192.168.0.0"/16;
        "172.16.0.0"/12;
	"10.0.0.0"/8;
}

sub vcl_recv {
        # allow PURGE from localhost and all local networks (RFC 1918)

        if (req.method == "PURGE") {
                if (!client.ip ~ purge) {
                        return(synth(405,"Not allowed."));
                }
                return (purge);
        }
}
